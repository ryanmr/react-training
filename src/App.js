import React, { Component } from 'react';
import {Link, IndexLink} from 'react-router';
import logo from './logo.svg';
import './App.css';
import Home from './modules/Home';

class NavigationLink extends Component {
  render() {
    return (
      <Link {...this.props} activeStyle={{color: 'red'}} />
    );
  }
}

function NavigationBar() {
  return (
    <div>
      <ul className="navigation">
        <li><IndexLink to="/" activeStyle={{color: 'red'}}>Home</IndexLink></li>
        <li><NavigationLink to="/about">About</NavigationLink></li>
        <li><NavigationLink to="/repos">Repos</NavigationLink></li>
        <li><NavigationLink to="/increment">Increment</NavigationLink></li>
      </ul>
    </div>
  );
}

class App extends Component {
  render() {
    return (
      <div className="App">
        <div className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h2>React Training</h2>
        </div>
        <NavigationBar />
        <div className="App-body">
          {this.props.children || <Home />}
        </div>
      </div>
    );
  }
}

export default App;
