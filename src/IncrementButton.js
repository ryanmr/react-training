import React, { Component } from 'react';

function DisplayCount(props) {
  return (
    <div>
      Count: {props.count}
    </div>
  );
}

class IncrementButton extends Component {
  constructor() {
    super();
    this.state = {
      count: 0
    };
  }

  render() {
    return (
      <div className="TestButtonContainer">
        <button onClick={this.increment.bind(this)}>Test Button</button>
        <br/>
        <DisplayCount count={this.state.count} />
      </div>
    );
  }

  increment() {
    const count = this.state.count + 1;
    this.setState({count: count});
  }
}

export default IncrementButton;
