import React, { Component } from 'react';
import DocumentTitle from 'react-document-title';

class About extends Component {
  render() {
    return (
      <DocumentTitle title="About">
        <div>
          <h3>About</h3>
        </div>
      </DocumentTitle>
    );
  }
}

export default About;
