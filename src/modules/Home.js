import React, { Component } from 'react';
import DocumentTitle from 'react-document-title';


class Home extends Component {
  render() {
    return (
      <DocumentTitle title="Home">
        <div>
          <h3>Home</h3>
        </div>
      </DocumentTitle>
    );
  }
}

export default Home;
