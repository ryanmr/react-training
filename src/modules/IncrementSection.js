import React, { Component } from 'react';
import IncrementButton from '../IncrementButton';
import DocumentTitle from 'react-document-title';

class IncrementSection extends Component {
  render() {
    return (
      <DocumentTitle title="Incrementing">
        <div>
          <h3>Incrementing Stuff</h3>
          <IncrementButton />
        </div>
      </DocumentTitle>
    );
  }
}

export default IncrementSection;
