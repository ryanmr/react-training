import React, { Component } from 'react';
import DocumentTitle from 'react-document-title';

class Repo extends Component {
  render() {
    return (
      <DocumentTitle title={this.props.params.repoName}>
        <div>
          <h2>{this.props.params.repoName}</h2>
        </div>
      </DocumentTitle>
    );
  }
}

export default Repo;
