import React, { Component } from 'react';
import {Link} from 'react-router';
import DocumentTitle from 'react-document-title';

class Repos extends Component {
  render() {
    return (
      <DocumentTitle title="Repositories">
        <div>
          <h3>Repos</h3>

          <ul className="no-bullets">
            <li><Link to="/repos/reactjs/react-router">React Router</Link></li>
            <li><Link to="/repos/facebook/react">React</Link></li>
          </ul>

          <div className="repo-body">
            {this.props.children}
          </div>
        </div>
      </DocumentTitle>
    );
  }
}

export default Repos;
