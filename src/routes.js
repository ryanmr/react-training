import React from 'react';
import {Route, IndexRoute} from 'react-router';

import App from './App';
import Home from './modules/Home';
import About from './modules/About';
import Repos from './modules/Repos';
import Repo from './modules/Repo';
import IncrementSection from './modules/IncrementSection';

module.exports = (
  <Route path="/" component={App}>

    <IndexRoute component={Home} />

    <Route path="/repos" component={Repos}>
      <Route path="/repos/:userName/:repoName" component={Repo} />
    </Route>
    <Route path="/about" component={About} />
    <Route path="/increment" component={IncrementSection} />
  </Route>
);
